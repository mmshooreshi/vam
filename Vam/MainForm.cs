﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Vam
{

    public static class ResourceManager
    {
        private static System.Resources.ResourceManager CResource = new System.Resources.ResourceManager("Vam.languages." + System.Globalization.CultureInfo.CurrentCulture.Name, System.Reflection.Assembly.GetExecutingAssembly());
        public static System.Resources.ResourceManager Resource { get { try { if (String.IsNullOrEmpty(CResource.GetString("ApplicationName"))) { } } catch { CResource = new System.Resources.ResourceManager("Vam.languages." + "en-US", System.Reflection.Assembly.GetExecutingAssembly()); } return CResource; } set { CResource = value; } }
    }
    public partial class MainForm : Form
    {
        private object[] p;

        public MainForm()
        {
            
        }

        public MainForm(object[] p)
        {
            // TODO: Complete member initialization
            this.p = p;
            InitializeComponent();

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void mf_Load(object sender, EventArgs e)
        {

        }

        private void search_Click(object sender, EventArgs e)
        {
            this.Hide();
            Vam.Search srch = new Vam.Search(new Object[] { "hohoho"
                                        });
            srch.Show();
        }

        private void about_Click(object sender, EventArgs e)
        {
            this.Hide();
            Vam.About abo = new Vam.About(new Object[] { "hohoho"
                                        });
            abo.Show();
        }
    }
}
